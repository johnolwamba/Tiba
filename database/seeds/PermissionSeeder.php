<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'manage-system'
        ];

        foreach ($permissions as $permission){
            $new_permission = Permission::firstOrCreate(['guard_name' => 'web','name' => $permission]);
        }

    }
}
