<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //admin
        $admin = Role::firstOrCreate([
            'name' => 'Admin'
        ]);

        $admin_permissions = [
            'manage-system',
        ];

        foreach ($admin_permissions as $admin_permission){

            if(!$admin->hasPermissionTo($admin_permission)) {
                $admin->givePermissionTo($admin_permission);

            }
        }


        $user = \App\User::firstOrCreate([
            'name' => 'System Administrator',
            'id_number' => '29722724',
            'email' => 'johnolwamba@gmail.com',
            'password' => bcrypt('secret'),
            'dob' => '22/6/1993',
            'county' => 'Vihiga',
            'phone_number' => '0718694198',
        ]);


        $user = \App\User::firstOrCreate([
            'name' => 'System Administrator',
            'id_number' => '29722722',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('secret'),
            'dob' => '22/6/1993',
            'county' => 'Vihiga',
            'phone_number' => '0718694190',
        ]);

    }
}
