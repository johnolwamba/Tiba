<?php

namespace App\Http\Controllers;

use App\Record;
use App\Risk;
use App\Symptom;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('email','!=','admin@gmail.com')->get();
        $records = Record::all();
        $risks = Risk::all();
        $symptoms = Symptom::all();
        return view('home',['users'=>$users,'records'=>$records,'risks'=>$risks,'symptoms'=>$symptoms]);
    }


    public function logout(){
        Auth::logout();
        return view('auth.login');
    }

    public function getUsers(){
        $users = User::where('email','!=','admin@gmail.com')->orderBy('id','desc')->get();
        return view('users',['users'=>$users]);
    }

    public function getSymptoms(){
        $symptoms = Symptom::orderBy('id','desc')->get();
        return view('symptoms',['symptoms'=>$symptoms]);
    }

    public function getRisks(){
        $risks = Risk::orderBy('id','desc')->get();
        return view('risks',['risks'=>$risks]);
    }

    public function getRecords(){
        $records = Record::orderBy('id','desc')->get();
        return view('records',['records'=>$records]);
    }


}
