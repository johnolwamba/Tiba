<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function () {

    //users
    Route::get('/users', [
        'as' => 'users', 'uses' => 'HomeController@getUsers'
    ]);

    Route::get('/logout',  [
        'as' => 'logout', 'uses' => 'HomeController@logout'
    ]);

    Route::get('/home',  [
        'as' => 'home', 'uses' => 'HomeController@index'
    ]);

    Route::get('/',  [
        'as' => 'home', 'uses' => 'HomeController@index'
    ]);

    //reports
    Route::get('/symptoms',  [
        'as' => 'symptoms', 'uses' => 'HomeController@getSymptoms'
    ]);


    //reports
    Route::get('/risks',  [
        'as' => 'risks', 'uses' => 'HomeController@getRisks'
    ]);

    //reports
    Route::get('/records',  [
        'as' => 'records', 'uses' => 'HomeController@getRecords'
    ]);


});
