@extends('layouts.master')

@section('content')
    <div class="page-wrapper">
        <div class="content container-fluid">
            <div class="row">
                <div class="col-xs-8">
                    <h4 class="page-title">Symptom Report</h4>
                </div>
            </div>

            <div class="filters">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-3 text-white">From:</div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <div class="input-group date" id="close_from">
                                            <input type="text" class="form-control" id="close-from">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-3 text-white">To:</div>
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <div class="input-group date" id="close_to">
                                            <input type="text" class="form-control" id="close-to">
                                            <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4"></div>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-striped custom-table m-b-0" id="reportTable">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Coughing Weeks</th>
                                <th>Coughing Blood</th>
                                <th>Weight Loss</th>
                                <th>Fever</th>
                                <th>Sweating</th>
                                <th>Weakness Fatigue</th>
                                <th>Creation Date</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($symptoms as $symptom)
                                <tr>
                                    <td>{{$symptom->user->name}}</td>
                                    <td>@if($symptom->coughingweeks == "1") <span class="btn btn-danger btn-block">Yes</span> @else <span class="btn btn-success btn-block">No</span> @endif</td>
                                    <td>@if($symptom->coughingblood == "1") <span class="btn btn-danger btn-block">Yes</span> @else <span class="btn btn-success btn-block">No</span> @endif</td>
                                    <td>@if($symptom->weightloss == "1") <span class="btn btn-danger btn-block">Yes</span> @else <span class="btn btn-success btn-block">No</span> @endif</td>
                                    <td>@if($symptom->fever == "1") <span class="btn btn-danger btn-block">Yes</span> @else <span class="btn btn-success btn-block">No</span> @endif</td>
                                    <td>@if($symptom->sweating == "1") <span class="btn btn-danger btn-block">Yes</span> @else <span class="btn btn-success btn-block">No</span> @endif</td>
                                    <td>@if($symptom->weaknessfatigue == "1") <span class="btn btn-danger btn-block">Yes</span> @else <span class="btn btn-success btn-block">No</span> @endif</td>
                                    <td>{{Carbon\Carbon::parse($symptom->created_at)->format('d-m-Y')}}</td>
                                    <td><a href="#" class="btn btn-primary">View</a> </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@section('scripts')
    <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.slimscroll.js"></script>


    <script src="{{asset('js/moment.min.js')}}"></script>
    <script src="{{asset('js/datetime-moment.js')}}"></script>
    <script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var reportTable = $('#reportTable').DataTable({
                responsive: true,
                "ordering": false,
                buttons: ['print', 'excel', 'pdf'],
            });

            $('#close_from').datetimepicker({format: 'DD-MM-YYYY'}).on("dp.change", function (e) {
                $('#close_to').data("DateTimePicker").minDate(e.date);
                reportTable.draw();
            });
            $('#close_to').datetimepicker({format: 'DD-MM-YYYY'}).on("dp.change", function (e) {
                reportTable.draw();
            });

        });

        $.fn.dataTable.moment('DD-MM-YYYY');
        //Report datatable
        $.fn.dataTable.ext.search.push(
            function( settings, data, dataIndex ) {
                if (settings.nTable.id != "reportTable") return true;
                date_from = moment('01-01-1900','DD-MM-YYYY');
                the_date = moment().format('DD-MM-YYYY');
                date_to = moment().endOf('year');

                if($('#close-from').val() != ""){
                    date_from = moment($('#close-from').val(),'DD-MM-YYYY');
                }
                if($('#close-to').val() != ""){
                    date_to = moment($('#close-to').val(),'DD-MM-YYYY');
                }
                if(data[3] != ""){
                    the_date = data[7];
                    console.log(the_date);
                }
                var loc = moment(the_date,'DD-MM-YYYY');
                // console.log(loc);
                // console.log(date_from);

                if (loc.isSameOrAfter(date_from) && loc.isSameOrBefore(date_to))
                {
                    console.log("true");
                    return true;
                }else{
                    console.log("false");
                    console.log(date_from);
                    console.log(loc);
                    return false;
                }
            }
        );


        // $.fn.dataTable.ext.search.push(
        //     function( settings, data, dataIndex ) {
        //         if (settings.nTable.id != "reportTable") return true;
        //         var value = $('#taskstatus').val();
        //         var d = data[7];
        //         if (value == null)
        //         {
        //             return true;
        //         }
        //         else if(value.indexOf(d) != -1){
        //             return true;
        //         }
        //         return false;
        //     }
        // );

    </script>


@endsection
@endsection
