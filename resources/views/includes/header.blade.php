<div class="header">
    <div class="header-left" style="margin-top: 20px;">
        {{--<a href="{{url('/')}}" class="logo">--}}
            {{--<img src="{{asset('images/logo2.png')}}" width="40" height="40" alt="">--}}
        {{--</a>--}}
        {{--<a href="{{url('/')}}" class="logo-dark">--}}
            {{--<img src="{{asset('images/logo2.png')}}" width="40" height="40" alt="">--}}
        {{--</a>--}}
        <h3>TIBA</h3>
    </div>
    <div class="page-title-box pull-left">

    </div>
    <a id="mobile_btn" class="mobile_btn pull-left" href="#sidebar"><i class="fa fa-bars" aria-hidden="true"></i></a>
    <ul class="nav navbar-nav navbar-right user-menu pull-right">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle user-link" data-toggle="dropdown" title="{{Auth::user()->name}}">
							<span class="user-img"><img class="img-circle" src="{{asset('images/user.jpg')}}" width="40" alt="{{Auth::user()->name}}">
							<span class="status online"></span></span>
                <span>{{Auth::user()->name}}</span>
                <i class="caret"></i>
            </a>
            <ul class="dropdown-menu">
                <li><a href="#">My Profile</a></li>
                <li><a href="{{url('/logout')}}">Logout</a></li>
            </ul>
        </li>
    </ul>
</div>