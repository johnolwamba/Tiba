<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
        <div id="sidebar-menu" class="sidebar-menu">
            <ul>
                @php
                    $active="";
                       if (\Request::is('home') || \Request::is('/'))
                           $active='active';
                @endphp
                <li class="nav-item {{$active}}">
                    <a href="{{url('/home')}}">Dashboard</a>
                </li>

                @php
                    $active="";
                       if (\Request::is('users'))
                           $active='active';
                @endphp
                <li class="nav-item {{$active}}">
                    <a href="{{url('users')}}">TB Contacts</a>
                </li>

                @php
                    $active="";
                       if (\Request::is('symptoms'))
                           $active='active';
                @endphp
                <li>
                    <a href="{{url('symptoms')}}">Symptomatic Contacts</a>
                </li>

                @php
                    $active="";
                       if (\Request::is('risks'))
                           $active='active';
                @endphp
                <li class="nav-item {{$active}}">
                    <a href="{{url('risks')}}">Contacts at Risk</a>
                </li>

                @php
                    $active="";
                       if (\Request::is('records'))
                           $active='active';
                @endphp
                <li class="nav-item {{$active}}">
                    <a href="{{url('records')}}">Summary Report</a>
                </li>

                <li>
                    <a href="{{url('logout')}}">Logout</a>
                </li>

            </ul>
        </div>
    </div>
</div>