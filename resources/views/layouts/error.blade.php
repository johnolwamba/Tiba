<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="shortcut icon" href="{{asset('resources/img/favicon.ico')}}" />
    <link rel="stylesheet" href="{{asset('vendor/iCheck/all.css')}}" />

    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
    <link rel="stylesheet" href="//cdn.datatables.net/responsive/2.0.0/css/responsive.dataTables.min.css">

    <!-- Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{asset('resources/icons/themify-icons/themify-icons.css')}}" rel="stylesheet">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('resources/css/main-style.min.css')}}">
    <link rel="stylesheet" href="{{asset('resources/css/skins/all-skins.min.css')}}">
    <link rel="stylesheet" href="{{asset('resources/css/demo.css')}}">
    <!--animate css-->
    <link rel="stylesheet" href="{{asset('vendor/animate.css')}}">

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-112423372-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-112423372-1');
    </script>
</head>
<body class="skin-blue error-page">

@yield('content')

<!-- Scripts -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="{{asset('vendor/fastclick/fastclick.min.js')}}"></script>
<script src="{{asset('vendor/sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('resources/js/demo.js')}}"></script>

@yield('scripts')

</body>
</html>
