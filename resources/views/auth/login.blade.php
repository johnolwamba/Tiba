@extends('layouts.app')

@section('content')

    <div class="main-wrapper">
        <div class="account-page">
            <div class="container">
                <h3 class="account-title">Welcome!</h3>
                <div class="account-box">
                    <div class="account-wrapper">
                        <div class="account-logo">
                            <a href="index.html"><img src="images/logo2.png" alt="Focus Technologies"></a>
                        </div>
                        <form  method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            <div class="form-group form-focus">
                                <label class="control-label">Email</label>
                                    <input id="email" type="email" class="floating form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                            </div>
                            <div class="form-group form-focus">
                                <label class="control-label">Password</label>
                                    <input id="password" type="password" class="floating form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                            </div>
                            <div class="form-group text-center">
                                <button class="btn btn-primary btn-block account-btn" type="submit">Login</button>
                            </div>
                            <div class="text-center">
                                <a href="#">Forgot your password?</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
