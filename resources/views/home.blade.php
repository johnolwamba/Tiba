@extends('layouts.master')

@section('content')
    <div class="page-wrapper">
        <div class="content container-fluid">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-lg-3">
                    <div class="dash-widget clearfix card-box">
                        <span class="dash-widget-icon"><i class="fa fa-users" aria-hidden="true"></i></span>
                        <div class="dash-widget-info">
                            <h3>{{count($users)}}</h3>
                            <h6>TB Contacts</h6>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-lg-3">
                    <div class="dash-widget clearfix card-box">
                        <span class="dash-widget-icon"><i class="fa fa-stethoscope" aria-hidden="true"></i></span>
                        <div class="dash-widget-info">
                            <h3>{{count($symptoms)}}</h3>
                            <h6>Symptomatic Contacts</h6>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-lg-3">
                    <div class="dash-widget clearfix card-box">
                        <span class="dash-widget-icon"><i class="fa fa-stethoscope"></i></span>
                        <div class="dash-widget-info">
                            <h3>{{count($risks)}}</h3>
                            <h6>Contacts at Risk</h6>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-lg-3">
                    <div class="dash-widget clearfix card-box">
                        <span class="dash-widget-icon"><i class="fa fa-file" aria-hidden="true"></i></span>
                        <div class="dash-widget-info">
                            <h3>{{count($records)}}</h3>
                            <h6>Summary Report</h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <div class="card-box">
                                <div id="area-chart" ></div>
                            </div>
                        </div>
                        <div class="col-sm-6 text-center">
                            <div class="card-box">
                                <div id="line-chart"></div>
                            </div>
                        </div>
                        <div  class="col-md-4 col-sm-12 text-center">
                            <div class="card-box">
                                <div id="bar-chart" ></div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 text-center">
                            <div class="card-box">
                                <div id="stacked" ></div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12 text-center">
                            <div class="card-box">
                                <div id="pie-chart" ></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
